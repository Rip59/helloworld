package ru.kvi.hello;

/**
 *Класс, позволяющий вывести на экран текст приветствия
 *
 *@author Кашников Владислав, 16ИТ18к
 */
public class Hello {
    public static void main(String[] args) {
        System.out.println("Hello, world");
    }
}
